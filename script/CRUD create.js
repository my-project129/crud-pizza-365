$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Biến lưu dữ liệu đơn hàng
    var gOrderData = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
    };

    var gDiscount = 0;

    var gSelectedDrink = {
        idLoaiNuocUong: "",
        tenNuocUong: "không có",
    };

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    // Khi bấm nút thêm mới đơn hàng
    $("#btn-create-order").on("click", function () {
        onBtnCreateClick();
    })

    // Khi thay đổi lựa chọn cỡ combo thì thay đổi giá trị của các ô select liên quan
    $("#slt-create-co-combo").on("change", function () {
        //console.log($("#slt-create-co-combo").val());
        onSelectKichCoChange();
    })

    // Khi bấm nút Create trong modal để thêm mới đơn hàng
    $("#btn-create-confirm").on("click", function () {
        onBtnCreateModalClick();
    })

    // Khi bấm Tạo đơn
    $("#btn-tao-don").on("click", function () {
        onBtnTaoDonClick();
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Khi bấm nút thêm mới đơn hàng
    function onBtnCreateClick() {
        "use strict";
        // Hiển thị modal lên
        $("#modal-create").modal("show");
    }

    // Khi thay đổi lựa chọn cỡ combo thì thay đổi giá trị của các ô select liên quan
    function onSelectKichCoChange() {
        "use strict";
        var vKichCo = $("#slt-create-co-combo").val();
        //console.log(vKichCo);
        switch (vKichCo) {
            case "S":
                $("#slt-create-duong-kinh-pizza").val(20);
                $("#slt-create-suon-nuong").val(2);
                $("#slt-create-salad").val(200);
                $("#slt-create-so-luong-nuoc").val(2);
                $("#slt-create-thanh-tien").val(150000);
                break;
            case "M":
                $("#slt-create-duong-kinh-pizza").val(25);
                $("#slt-create-suon-nuong").val(4);
                $("#slt-create-salad").val(300);
                $("#slt-create-so-luong-nuoc").val(3);
                $("#slt-create-thanh-tien").val(200000);
                break;
            default:
                $("#slt-create-duong-kinh-pizza").val(30);
                $("#slt-create-suon-nuong").val(8);
                $("#slt-create-salad").val(500);
                $("#slt-create-so-luong-nuoc").val(4);
                $("#slt-create-thanh-tien").val(250000);
        }
    }

    // Khi bấm nút Create trong modal để thêm mới đơn hàng
    function onBtnCreateModalClick() {
        "use strict";
        // B1: Thu thập dữ liệu
        getGuiDonHangData(gOrderData);
        // B2: Kiểm tra dữ liệu
        var vIsDataValidate = validateData(gOrderData);
        if (vIsDataValidate) {
            // B3: Gọi api kiểm tra mã voucher
            if (gOrderData.idVourcher != "") {
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + gOrderData.idVourcher,
                    type: "GET",
                    success: function (res) {
                        console.log(res);
                        // Lấy giá trị phần trăm giảm giá
                        gDiscount = res.phanTramGiamGia;
                        // B4: Xử lí hiển thị
                        showGuiDonHangInfo();
                    },
                    error: function (res) {
                        console.log(res);
                        alert("Không tìm thấy mã voucher");
                    }
                })
            } else {
                // B4: Xử lí hiển thị 
                showGuiDonHangInfo();
            }
        }

    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Thu thập dữ liệu modal create
    function getGuiDonHangData(paramOrderData) {
        "use strict";
        paramOrderData.kichCo = $("#slt-create-co-combo").val();
        paramOrderData.duongKinh = $("#slt-create-duong-kinh-pizza").val();
        paramOrderData.suon = $("#slt-create-suon-nuong").val();
        paramOrderData.salad = $("#slt-create-salad").val();
        paramOrderData.soLuongNuoc = $("#slt-create-so-luong-nuoc").val();
        paramOrderData.thanhTien = $("#slt-create-thanh-tien").val();
        paramOrderData.loaiPizza = $("#slt-create-loai-pizza").val();
        paramOrderData.hoTen = $("#inp-create-full-name").val().trim();
        paramOrderData.email = $("#inp-create-email").val().trim();
        paramOrderData.soDienThoai = $("#inp-create-phone-number").val();
        paramOrderData.diaChi = $("#inp-create-address").val().trim();
        paramOrderData.loiNhan = $("#inp-create-message").val().trim();
        paramOrderData.idVourcher = $("#inp-create-voucher-id").val();
        paramOrderData.idLoaiNuocUong = $("#slt-create-do-uong").val();

        console.log(paramOrderData);
    }

    // Hàm kiểm tra dữ liệu nhập vào ở modal create
    function validateData(paramOrderData) {
        "use strict";
        var format = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;

        if (paramOrderData.hoTen == "") {
            alert("Vui lòng nhập vào Họ và Tên");
            return false;
        }
        if (format.test(paramOrderData.hoTen)) {
            alert("Họ và Tên không được chứa ký tự đặc biệt");
            return false;
        }
        if ((paramOrderData.email != "") && (paramOrderData.email.includes("@") == false)) {
            alert("Vui lòng nhập vào email hợp lệ");
            return false;
        }
        if (paramOrderData.soDienThoai == "") {
            alert("Vui lòng nhập vào số điện thoại");
            return false;
        }
        if (paramOrderData.diaChi == "") {
            alert("Vui lòng nhập vào địa chỉ");
            return false;
        }
        return true;
    }

    // Khai báo hàm chọn loại nước uống
    function onSelectDrinkChange() {
        var vSelectDrinkElement = $("#select-drink");
        gOrderData.idLoaiNuocUong = $("#select-drink").val();
        var vIdLoaiNuocUong = gOrderData.idLoaiNuocUong;

        if (vIdLoaiNuocUong != "0") {
            for (let bI = 0; bI < gDrinkObject.length; bI++) {
                if (vIdLoaiNuocUong == gDrinkObject[bI].maNuocUong) {
                    gSelectedDrink = gDrinkObject[bI];
                }
            }
        } else {
            gSelectedDrink.tenNuocUong = null;
            gSelectedDrink.idLoaiNuocUong = 0;
        }
        console.log("Nước uống được chọn: ")
        console.log(gSelectedDrink);
        console.log(gOrderData);
    }

    // Thông báo hiển thị sau khi bấm Create hàng trong modal create
    function showGuiDonHangInfo() {
        "use strict";
        // Hiển thị thông tin lên form
        $("#modal-inp-full-name").val(gOrderData.hoTen);
        $("#modal-inp-phone-number").val(gOrderData.soDienThoai);
        $("#modal-inp-address").val(gOrderData.diaChi);
        $("#modal-inp-message").val(gOrderData.loiNhan);
        $("#modal-inp-voucherid").val(gOrderData.idVourcher);

        var vModalTextArea = "Xác nhận: "
            + gOrderData.hoTen + ", " + gOrderData.soDienThoai + ", " + gOrderData.diaChi + "\n"
            + "Menu " + gOrderData.kichCo + ", sườn nướng " + gOrderData.suon + ", nước " + gOrderData.soLuongNuoc + ", " + gOrderData.idLoaiNuocUong + "\n"
            + "Loại pizza: " + gOrderData.loaiPizza + ", Giá: " + gOrderData.thanhTien + " vnd, mã giảm giá: " + gOrderData.idVourcher + "\n"
            + "Phải thanh toán: " + getPayment(gDiscount); + "vnd (Giảm giá " + gDiscount + "%)"

        $("#modal-txt-are").text(vModalTextArea);

        // Bật modal 
        $("#exampleModalLong").modal("show");
    }

    // Hàm tính giá tiền phải trả sau khi đã nhận giảm giá
    function getPayment(paramDiscount) {
        "use strict";
        return gOrderData.thanhTien * (100 - paramDiscount) / 100
    }

    // Khai báo hàm xử lí bấm nút Tạo Đơn
    function onBtnTaoDonClick() {
        "use strict";
        // B1: Thu thập dữ liệu (bỏ qua)
        // B2: Kiểm tra dữ liệu (bỏ qua)
        // B3: Gọi Api tạo đơn hàng
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            data: JSON.stringify(gOrderData),
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            success: function (res) {
                console.log(res);
                // B4: Xử lí hiển thị
                // Tắt modal 
                $("#exampleModalLong").modal("hide");
                // Hiển thị vùng thông báo Cảm Ơn
                $("#exampleModalResult").modal("show");
                //$("#div-container-thanks").prop("style","display: block");
                $("#div-orderid").val(res.orderId);
            },
            error: function (res) {
                console.log(res);
            }
        })
    }
})